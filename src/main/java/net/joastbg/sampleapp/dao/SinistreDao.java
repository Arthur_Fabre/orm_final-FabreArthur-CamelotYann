package net.joastbg.sampleapp.dao;

import net.joastbg.sampleapp.entities.Sinistre;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SinistreDao {
    
    @Autowired
    public SessionFactory sessionFactory;
 
    //Cette class g�n�rique contient toutes les fonctionnalit� de percistance et commune � toutes les DAO
    public AddUpdateDeleteDao<Sinistre,Integer> maj = new AddUpdateDeleteDao<Sinistre,Integer>("Sinistre");
    
    
}
