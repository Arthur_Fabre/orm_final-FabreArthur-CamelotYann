package net.joastbg.sampleapp;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import net.joastbg.sampleapp.dao.AssuranceDao;
import net.joastbg.sampleapp.entities.Assurance;
import net.joastbg.sampleapp.entities.AssuranceAuto;
import net.joastbg.sampleapp.entities.AssuranceHabitat;

@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=false)
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring/config/BeanLocations.xml")
public class AssuranceDaoTest {

    @Autowired
    public AssuranceDao assuranceDao;

    @Before
    public void setUp() {
        Assert.assertTrue(true);
    }
    
    @Test
    public void testFindAll(){
	List<Assurance> assurance = assuranceDao.maj.findAll(assuranceDao.sessionFactory);
	Assert.assertNotNull(assurance);
    }
    
    @Test
    public void testFindAllPP(){
	List<Assurance> assurance = assuranceDao.findAH();
	Assert.assertNotNull(assurance);
    }
    
    @Test
    public void testFindAllPM(){
	List<Assurance> assurance = assuranceDao.findAA();
	Assert.assertNotNull(assurance);
    }
}
