package net.joastbg.sampleapp.dao;

import java.util.Date;
import java.util.List;
import net.joastbg.sampleapp.entities.Client;
import net.joastbg.sampleapp.entities.Echeances;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EcheancesDao {
    
    @Autowired
    public SessionFactory sessionFactory;
 
    //Cette class g�n�rique contient toutes les fonctionnalit� de percistance et commune � toutes les DAO
    public AddUpdateDeleteDao<Echeances,Integer> maj = new AddUpdateDeleteDao<Echeances,Integer>("Echeances");
    
    public List<Echeances> getEcheancier(Client client){
        Session session = sessionFactory.getCurrentSession();
        return this.maj.findWithParameters("where assuranceLiee="+client.getIdClient(), this.sessionFactory);
    }
    
    
    
}
