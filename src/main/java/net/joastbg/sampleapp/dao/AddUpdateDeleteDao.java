package net.joastbg.sampleapp.dao;

import java.lang.ProcessBuilder.Redirect.Type;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

//C'est dans cette class que ce trouve les fonctions en lien avec la percistance. C'est une classe g�n�rique qui prendra deux �l�ments en param�tres
//T : Qui correspond � l'entite lier au DAO
//S : Qui est le format de la clef primaire de l'entit�

@Service
@Transactional
public class AddUpdateDeleteDao<T,S> {
    
    private String table;
    
    public AddUpdateDeleteDao(){}

    //Constructeur ou le nom de la table estpass�e en param�tres
    public AddUpdateDeleteDao(String table){
        this.table=table;
    }
    
    //Fonction qui ins�re les donn�es en base
    public S save(T object,SessionFactory sessionFactory){ //marche pas avec les sequences
        Session session = sessionFactory.getCurrentSession();
        S returnID = (S) session.save(object);
        return returnID;
    }
    
    //Fonction qui met � jours les donn�es
    public String update(T object,SessionFactory sessionFactory){
        Session session = sessionFactory.getCurrentSession();
        session.update(object);
        return "Session Update";
    }
    
    //Fonction qui supprime une donn�es de la base
    public String delete(T object,SessionFactory sessionFactory){
        Session session = sessionFactory.getCurrentSession();
        session.delete(object);
        return "Session Delete";
    }
    
    //Fonction commune � toute les DAO elle permet de lister tous les tuple d'une table
    public List<T> findAll(SessionFactory sessionFactory){
        Session session = sessionFactory.getCurrentSession(); 
        return  session.createQuery("from "+this.table).list();
    }
    
    //Fonction commune � toute les DAO elle permet de lister tous les tuple d'une table en fonction de param�tres
    public List<T> findWithParameters(String condition,SessionFactory sessionFactory){
        Session session = sessionFactory.getCurrentSession();
        return  session.createQuery("from "+this.table+" where "+condition).list();
    }
}
