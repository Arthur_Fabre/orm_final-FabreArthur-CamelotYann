package net.joastbg.sampleapp;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import net.joastbg.sampleapp.dao.ContactDao;
import net.joastbg.sampleapp.entities.Contact;
import net.joastbg.sampleapp.entities.TypeContact;

@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=false)
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring/config/BeanLocations.xml")
public class ContactDaoTest {

    @Autowired
    ContactDao contactDao;

    @Before
    public void setUp() {
        Assert.assertTrue(true);
    }

       @Test
    public void testFindAll(){
	List<Contact> contact = contactDao.maj.findAll(contactDao.sessionFactory);
	Assert.assertNotNull(contact);
    }
    



}
