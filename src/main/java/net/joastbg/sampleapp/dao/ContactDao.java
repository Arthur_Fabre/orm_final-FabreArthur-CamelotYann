package net.joastbg.sampleapp.dao;

import java.util.List;
import net.joastbg.sampleapp.entities.Contact;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ContactDao {
    
    @Autowired
    public SessionFactory sessionFactory;

    //Cette class g�n�rique contient toutes les fonctionnalit� de percistance et commune � toutes les DAO
    public AddUpdateDeleteDao<Contact,Integer> maj = new AddUpdateDeleteDao<Contact,Integer>("Contact");
    
}
