package net.joastbg.sampleapp;

import java.util.ArrayList;
import junit.framework.Assert;
import net.joastbg.sampleapp.dao.ClientDao;
import net.joastbg.sampleapp.entities.Client;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import net.joastbg.sampleapp.dao.CompteBancaireDao;
import net.joastbg.sampleapp.entities.CompteBancaire;
import net.joastbg.sampleapp.entities.PersonneMorale;
import net.joastbg.sampleapp.entities.PersonnePhysique;

@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=false)
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring/config/BeanLocations.xml")
public class ClientDaoTest {

    @Autowired
    ClientDao clientDao;

    @Before
    public void setUp() {
        Assert.assertTrue(true);
    }

    @Test
    public void testFindAll(){
	List<Client> client = clientDao.maj.findAll(clientDao.sessionFactory);
	Assert.assertNotNull(client);
    }
    
    @Test
    public void testFindAllPP(){
	List<Client> clientPP = clientDao.findPP();
	Assert.assertNotNull(clientPP);
    }
    
    @Test
    public void testFindAllPM(){
	List<Client> clientPM = clientDao.findPM();
	Assert.assertNotNull(clientPM);
    }
    
   



}
