package net.joastbg.sampleapp.dao;

import java.util.List;
import net.joastbg.sampleapp.entities.Client;
import net.joastbg.sampleapp.entities.CompteBancaire;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CompteBancaireDao {
    
    @Autowired
    public SessionFactory sessionFactory;

    //Cette class g�n�rique contient toutes les fonctionnalit� de percistance et commune � toutes les DAO
    public AddUpdateDeleteDao<CompteBancaire,String> maj = new AddUpdateDeleteDao<CompteBancaire,String>("CompteBancaire");
    
    //Retourne les comptes bancaires d'un client
    public List<CompteBancaire> getComptesClient(Client client){
        Session session = sessionFactory.getCurrentSession();
        return  session.createQuery("from CompteBancaire Where proprietaire="+client.getIdClient()).list();
    }
    
}
