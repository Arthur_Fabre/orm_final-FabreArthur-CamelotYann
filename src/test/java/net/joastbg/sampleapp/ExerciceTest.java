package net.joastbg.sampleapp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import junit.framework.Assert;
import net.joastbg.sampleapp.dao.AssuranceDao;
import net.joastbg.sampleapp.dao.ClientDao;
import net.joastbg.sampleapp.dao.CompteBancaireDao;
import net.joastbg.sampleapp.dao.ContactDao;
import net.joastbg.sampleapp.dao.EcheancesDao;
import net.joastbg.sampleapp.dao.PersonnePhysiqueDao;
import net.joastbg.sampleapp.dao.SinistreDao;
import net.joastbg.sampleapp.entities.Assurance;
import net.joastbg.sampleapp.entities.AssuranceAuto;
import net.joastbg.sampleapp.entities.AssuranceHabitat;
import net.joastbg.sampleapp.entities.Client;
import net.joastbg.sampleapp.entities.CompteBancaire;
import net.joastbg.sampleapp.entities.Contact;
import net.joastbg.sampleapp.entities.Echeances;
import net.joastbg.sampleapp.entities.PersonneMorale;
import net.joastbg.sampleapp.entities.PersonnePhysique;
import net.joastbg.sampleapp.entities.Sinistre;
import net.joastbg.sampleapp.entities.TypeContact;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;


@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=false)
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring/config/BeanLocations.xml")
public class ExerciceTest {

    @Autowired
    ClientDao clientDao;
    @Autowired
    AssuranceDao assuranceDao;
    @Autowired
    CompteBancaireDao comptebancaireDao;
    @Autowired
    PersonnePhysiqueDao personnePhysiqueDao;
    @Autowired
    EcheancesDao echeanceDao;
    @Autowired
    SinistreDao sinistreDao;
    @Autowired
    ContactDao contactDao;
    
    @Before
    public void setUp() {
        Assert.assertTrue(true);
    }

    ////////////////////////// CREATION DE CLIENT ////////////////////////// 
    @Test //Creation PersonnePhysique
    public void createPersonnePhysique() {
        System.out.println("\n--[1]CREATION DE CLIENT PP--");
        Integer id = clientDao.maj.save(new PersonnePhysique("nom","prenom","01/01/2001"), clientDao.sessionFactory); //Creation et insersion en table du client
        List<Client> lastCli= clientDao.maj.findWithParameters("idClient="+id, clientDao.sessionFactory); //Recherche du client cr�� en base
        Assert.assertNotNull(lastCli);
    }  
    
    @Test //Creation PersonneMorale
    public void createPersonneMorale() {
        System.out.println("\n--[1]CREATION DE CLIENT PM--");
        Integer id =clientDao.maj.save(new PersonneMorale("nom","siren"), clientDao.sessionFactory);
        List<Client> lastCli= clientDao.maj.findWithParameters("idClient="+id, clientDao.sessionFactory);
        Assert.assertNotNull(lastCli);
    }
    //////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////// AJOUT DE LA LISTE DE COMPTE D'UN CLIENT ////////////////////////// 
    @Test
    public void addListeCB() {
        System.out.println("\n--[2]AJOUT DE LA LISTE DE COMPTE D'UN CLIENT--");
        Integer id = clientDao.maj.save(new PersonnePhysique("Paul","Karunin","08/12/1981"), clientDao.sessionFactory); //Creation est r�cup�ration du client
        List<Client> lastCli= clientDao.maj.findWithParameters("idClient="+id, clientDao.sessionFactory);
	ArrayList<CompteBancaire> lcb = new ArrayList<CompteBancaire>();        //Creation de la liste des compte � ajouter
        lcb.add(new CompteBancaire("iban1","bic1",lastCli.get(0)));
        lcb.add(new CompteBancaire("iban2","bic2",lastCli.get(0)));
        lcb.add(new CompteBancaire("iban3","bic3",lastCli.get(0)));
        lcb=lastCli.get(0).addComptes(lcb);                                     //Ajout des compte
        boolean test=true;
        for(int i=0;i<lcb.size();i++){
            comptebancaireDao.maj.save(lcb.get(i), comptebancaireDao.sessionFactory);   //Sauvegarde des compte cr��
        }
        List<CompteBancaire> lb = comptebancaireDao.maj.findWithParameters("iban in ('iban1','iban2','iban3')",comptebancaireDao.sessionFactory);   //Verifiaction de la sauvegarde et de l'asignation
        for(CompteBancaire c : lb){
            if(c.getProprietaire().getIdClient()!= lastCli.get(0).getIdClient()) test=false;
        }
        Assert.assertEquals(test,true);
    }
    //////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////// DEFINIR COMPTE PRINCIPAL //////////////////////////
    @Test
    public void addClientCp() {
        System.out.println("\n--[3]DEFINIR COMPTE PRINCIPAL--");
        Integer id = clientDao.maj.save(new PersonnePhysique("Paul","Karunin","08/12/1981"), clientDao.sessionFactory); //Creation est r�cup�ration du client
        List<Client> lastCli= clientDao.maj.findWithParameters("idClient="+id, clientDao.sessionFactory);
	CompteBancaire cp = new CompteBancaire("iban4","bic4",lastCli.get(0));
        comptebancaireDao.maj.save(cp, comptebancaireDao.sessionFactory);
        lastCli.get(0).addComptesPrincipal(cp);
        clientDao.maj.update(lastCli.get(0), clientDao.sessionFactory);
        lastCli= clientDao.maj.findWithParameters("idClient="+id, clientDao.sessionFactory);
        Assert.assertNotNull(lastCli.get(0).getIbanComptePrincipal());
    }
    //////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////// GERER LA LISTE DES CONTACTS //////////////////////////
    @Test //Creation d'un contact
    public void createContactList(){
        System.out.println("\n--[4]GERER LA LISTE DES COMTACTS--");
        List<Client> Cli= clientDao.maj.findWithParameters("idClient=1", clientDao.sessionFactory);                             //Recup�ration du premier client
        contactDao.maj.save(new Contact(Cli.get(0),TypeContact.FIXE,"0456826884"), contactDao.sessionFactory);                  //Creation du comtact
        List<Contact> con= contactDao.maj.findWithParameters("client="+Cli.get(0).getIdClient(), contactDao.sessionFactory);    //Verification que le contact � bien �t� cr��
        Assert.assertNotNull(con);
    }
    //////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////// CREER ET LIER UNE ASSURANCE //////////////////////////
    @Test //Creation AssuranceAuto
    public void creatAssuranceAuto() {
        System.out.println("\n--[5]Creation AssuranceAuto--");
        List<Client> Cli= clientDao.maj.findWithParameters("idClient=1", clientDao.sessionFactory);                                                                     //Recup�ration du premier client
        assuranceDao.maj.save(new AssuranceAuto(new Date(2000,5,8),new Date(2000,5,8),new Date(2000,5,8), Cli.get(0), "15-123-EZ", 2), assuranceDao.sessionFactory);    //Creation de l'assurance
        List<Assurance> aa= assuranceDao.maj.findWithParameters("idAssure="+Cli.get(0).getIdClient(), contactDao.sessionFactory);                                       //Verification que l'assurance � bien �t� cr��
        Assert.assertNotNull(aa);
    }     
    
    @Test //Creation AssuranceHabitation
    public void creatAssuranceHabitat() {
        System.out.println("\n--[5]Creation AssuranceHabitation--");
        List<Client> Cli= clientDao.maj.findWithParameters("idClient=1", clientDao.sessionFactory);                                                                                 //Recup�ration du premier client
	assuranceDao.maj.save(new AssuranceHabitat(new Date(2000,5,8),new Date(2000,5,8),new Date(2000,5,8), Cli.get(0), "10 avenue montpellier", 2), assuranceDao.sessionFactory); //Creation de l'assurance
        List<Assurance> aa= assuranceDao.maj.findWithParameters("idAssure="+Cli.get(0).getIdClient(), contactDao.sessionFactory);                                                   //Verification que l'assurance � bien �t� cr��
        Assert.assertNotNull(aa);
    } 
    
    @Test //Ajout Conducteur secondaire
    public void addOtherConductor() {
        System.out.println("\n--[5]Ajout Conducteur secondaire--");
        List<Client> client = clientDao.findPP();                                                                       //Listage de toute les personnes physique
        Integer id = clientDao.maj.save(new PersonnePhysique("Stetford","Bob","01/01/2001"), clientDao.sessionFactory); //Creation d'une personne physique
        List<Client> lastCli= clientDao.maj.findWithParameters("idClient="+id, clientDao.sessionFactory);               //Recherche du client cr�� en base
        assuranceDao.maj.save(new AssuranceAuto(new Date(2000,5,8),new Date(2000,5,8),new Date(2000,5,8), lastCli.get(0), "15-123-EZ", 2), assuranceDao.sessionFactory);    //Creation de l'assurance
        List<Assurance> aa= assuranceDao.maj.findWithParameters("idAssure="+lastCli.get(0).getIdClient(), contactDao.sessionFactory);                                       //Recuperation de la derni�re assurance dans la base
	aa.get(0).setAssures(client);                                                                                       //Assignation de la liste des assur� secondaire
        Integer idAssurance = aa.get(0).getIdAssurance();                                                               
        assuranceDao.maj.update(aa.get(0), contactDao.sessionFactory);                                                      //Mise � jour de l'assurance
        List<Assurance> aaNew= assuranceDao.maj.findWithParameters("idAssurance="+idAssurance, contactDao.sessionFactory);  //Verification que les deux liste sont bien egales
        Assert.assertEquals(client, aaNew.get(0).getAssures());
    }     
    ////////////////////////// CREER ET LIER UNE ECHEANCE //////////////////////////
    @Test
    public void createcheance() {
        System.out.println("\n--[6]CREE ET LIER UNE ECHEANCE--");
        List<Assurance> aaNew= assuranceDao.maj.findAll(contactDao.sessionFactory);                                         //Recuperation d'une assurance
        Integer id = echeanceDao.maj.save(new Echeances(300,new Date(2010,5,8),aaNew.get(0)), echeanceDao.sessionFactory);  //Creation d'une echeance          
        List<Echeances> e= echeanceDao.maj.findWithParameters("idEcheance="+id, echeanceDao.sessionFactory);                //Verification que l'echeancd � bien �t� cr��
        Assert.assertNotNull(e);
    }
    //////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////// ECHEANCIER A 6 MOIS //////////////////////////
    @Test
    public void getEcheancierTest(){
        System.out.println("\n--[7]ECHEANCIER A 6 MOIS--");
        List<Client> client = clientDao.maj.findWithParameters("idClient = 1", clientDao.sessionFactory);   //Recuperation du premier client
        Map<String, Assurance> assurances = this.assuranceDao.findSixMonth(client.get(0));                  //Requette recuuperation de son �ch�ancier
        Assert.assertNotNull(assurances);                                                                   //Verification qu'il n'est pas null
        Set<Entry<String, Assurance>> setAssurance = assurances.entrySet();                                 //Formatage et affichage
        Iterator<Entry<String, Assurance>> it2 = setAssurance.iterator();
        while(it2.hasNext()){
           Entry<String, Assurance> e = it2.next();
           System.out.println("Mois : "+e.getKey() + "\n\tId de l'assurance : " + e.getValue().getIdAssurance());
           List<Echeances> echeances = this.echeanceDao.maj.findWithParameters("assuranceLiee="+e.getValue().getIdAssurance(), echeanceDao.sessionFactory);
           for(Echeances echeance : echeances){
               System.out.println("\t"+echeance.getPrix()+"� Pay�->"+echeance.getDatePaiement());
           }
        }
    }
    //////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////// AJOUT SINISTRE //////////////////////////
    @Test
    public void addSinistreAH(){
        System.out.println("\n--[8]AJOUT SINISTRE AH--");
        List<Assurance> assurance = assuranceDao.maj.findAll(assuranceDao.sessionFactory);                                                              //Recuperation des assurance
        Integer id = sinistreDao.maj.save(new Sinistre(new Date(2018,06,30),"Inondation","Inondation",assurance.get(0)), sinistreDao.sessionFactory);   //Cr�ation d'un sinistre sur la premi�re
        List<Sinistre> sinistre= sinistreDao.maj.findWithParameters("idSinistre="+id, sinistreDao.sessionFactory);                                      //Verification que le sinistre � bien �t� cr��
        Assert.assertNotNull(sinistre);
    }
    
    @Test
    public void addSinistreAA(){
        System.out.println("\n--[8]AJOUT SINISTRE AA--");
        List<Assurance> assurance = assuranceDao.maj.findAll(assuranceDao.sessionFactory);
        Integer id = sinistreDao.maj.save(new Sinistre(new Date(2018,06,30),"Parchoc","Parchoc","Plaque",assurance.get(1)), sinistreDao.sessionFactory);   //Cr�ation d'un sinistre sur la premi�re
        List<Sinistre> sinistre= sinistreDao.maj.findWithParameters("idSinistre="+id, sinistreDao.sessionFactory);                                      //Verification que le sinistre � bien �t� cr��
        Assert.assertNotNull(sinistre);
    }
    //////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////// CLOTURE DES ASSURANCE //////////////////////////
    @Test
    public void deleteAssurance(){
        System.out.println("\n--[9]CLOTURE DES ASSURANCE--");
        List<Assurance> assurance = assuranceDao.findEndAssuance(new Date(2018-1900,05,05)); //Recupertation des assurance termin�e � une date
        Assert.assertEquals(1, assurance.size());
        for(Assurance a: assurance){                                                    //Supression des echeance et des comptes
            List<Echeances> echeances = echeanceDao.maj.findWithParameters("assuranceLiee="+a.getIdAssurance(), echeanceDao.sessionFactory);
            for(Echeances e: echeances){
                echeanceDao.maj.delete(e, echeanceDao.sessionFactory);
            }
            assuranceDao.maj.delete(a, assuranceDao.sessionFactory);
        }
        
        List<Assurance> assurance2 = assuranceDao.findEndAssuance(new Date(2018,06,05));    //Verification que tout a bien �tait supprim�
        Assert.assertEquals(0, assurance2.size());
        
        
    }
    //////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////// ASSURANCE DE 3 MOIS //////////////////////////
    @Test
    public void getMonthAssurance(){
        System.out.println("\n--[10]ASSURANCE DE 3 MOIS--");
        List<Client> Cli= clientDao.maj.findWithParameters("idClient=4", clientDao.sessionFactory);                                                                     //Recup�ration du premier client
        Date today= new Date();
        Date d1= new Date(); 
        Date d2= new Date(); 
        Date d3= new Date();
        d1.setMonth(d1.getMonth()+1);
        d2.setMonth(d2.getMonth()+2);
        d3.setMonth(d3.getMonth()+3);
        assuranceDao.maj.save(new AssuranceAuto(new Date(2000,5,8),d1,new Date(2000,5,8), Cli.get(0), "15-123-EZ", 2), assuranceDao.sessionFactory);    //Creation de l'assurance
        assuranceDao.maj.save(new AssuranceAuto(new Date(2000,5,8),d2,new Date(2000,5,8), Cli.get(0), "15-123-EZ", 2), assuranceDao.sessionFactory);    //Creation de l'assurance
        assuranceDao.maj.save(new AssuranceAuto(new Date(2000,5,8),d3,new Date(2000,5,8), Cli.get(0), "15-123-EZ", 2), assuranceDao.sessionFactory);    //Creation de l'assurance
        List lam = assuranceDao.findMonth();        //Recuperation de la liste des assurances
        Assert.assertEquals(3, lam.size());
    }
    //////////////////////////////////////////////////////////////////////////////
}
